package com.example.alumno.juanlivonpp;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

/**
 * Created by alumno on 09/05/2019.
 */

public class MyViewHolder extends ViewHolder {
    TextView nombreProducto;
    TextView cantidad;
    TextView precio;

    public MyViewHolder(View itemView) {
        super(itemView);
        this.nombreProducto = (TextView) itemView.findViewById(R.id.nombreProducto);
        this.cantidad = (TextView) itemView.findViewById(R.id.cantidad);
        this.precio = (TextView) itemView.findViewById(R.id.precio);
    }
}
