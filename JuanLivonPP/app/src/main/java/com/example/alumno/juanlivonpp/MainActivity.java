package com.example.alumno.juanlivonpp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Producto> productos = new ArrayList<Producto>();
        XmlParser xmlParser = new XmlParser();

        HttpConnection con = new HttpConnection();
        try {
            String xml = con.getStringData("http://192.168.2.166:8080/productos.xml");
            productos = xmlParser.obtenerProductos(xml);

            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
